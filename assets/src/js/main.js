;(function(context) {

	//load all required scripts	
	require('./scripts/anchors.external.popup.js');
	require('./scripts/standard.accordion.js');
	require('./scripts/custom.select.js');
	require('./scripts/date.inputs.js');
//	require('./scripts/aspect.ratio.js');
	require('./scripts/magnific.popup.js');
//	require('./scripts/responsive.video.js');
	require('./scripts/lazy.images.js');
	require('./scripts/tabs.js');
	require('./scripts/nav.js');
//	require('./scripts/blocks.js');
	require('./scripts/location.map.js');

	var tests;
	var debounce;
	var preventOverScroll;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		preventOverScroll = require('./scripts/preventOverScroll.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();
	preventOverScroll($('div.nav')[0]);
	
	//get a quote form
	$('div.get-a-quote').each(function() {
		
		var
			form = $('form',this),
			quoteSelector = $('.selector select',this),
			buttons = $('.buttons span',this),
			BUTTON_SELECTED_CLASS = 'selected';
		
		var methods = {
			
			setSelectedQuoteType: function(index) {
				quoteSelector[0].selectedIndex = index+1;
				$(document).trigger('updateTemplate.selector');
				buttons
					.removeClass(BUTTON_SELECTED_CLASS)
					.eq(index)
					.addClass(BUTTON_SELECTED_CLASS);
			}
			
		};
		
		buttons.on('click',function() { methods.setSelectedQuoteType(buttons.index(this)); })
		quoteSelector.on('change',function() { methods.setSelectedQuoteType(this.selectedIndex-1); })
		
		
		
	});
	
	//all generic faders/heros
	$('.fader').each(function() {
		var 
			
			slickEl,
			el = $(this),
			methods = {
				
				getElementWithSrcData: function(el) {
					return el.data('src') !== undefined ? el : el.find('.fader-item-bg').filter(function() { return $(this).data('src') !== undefined });
				},
				
				setImageOnElements: function(els,source) {
					els.each(function() {
						$(this)
							.css({backgroundImage: 'url('+source+')' })
							.addClass('loaded');
					});
				},
				
				loadImageForElementAtIndex: function(i) {
					var 
						self = this;
						element = $('.fader-item',el).eq(i),
						sourceElement = this.getElementWithSrcData(element),
						rawSource = sourceElement.data('src'),
						source = imageLoader.getAppropriateSource(rawSource),
						allElements = sourceElement.add(self.getElementWithSrcData(element.siblings()).filter(function() { 
							return $(this).data('src') === rawSource; 
						}));
					
						element.addClass('loading');
						
						//console.log(rawSource.split(','));
						
						if(!source) {
							element.addClass('loaded');
						}
						
						if(imageLoader.hasSourceLoaded(source)) {
							this.setImageOnElements(allElements,source);
							return;
						}
						
						imageLoader
							.loadSource(source)
							.then(function() {
								self.setImageOnElements(allElements,source);
								element.addClass('loaded');
							});
					
				}

			};
			
		el.slick({
			dots:true,
			appendDots:$('.fader-nav',el.parent()),
			appendArrows:$('.fader-controls',el.parent()),
			prevArrow: '<button class="prev"/>',
			nextArrow: '<button class="next"/>',
			draggable:false,
			swipe:true,
			touchMove:true,
			autoplay:true,
			autoplaySpeed: 5000,
			pauseOnHover: false,
			fade:!tests.touch()
		});
		
		el.on('beforeChange',function(slick,e,i) {
			methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));
		});
		
		methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));
		
		$(window).on('resize',function() {
			d.requestProcess(function() { methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide')); }); 
		})

	});
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));