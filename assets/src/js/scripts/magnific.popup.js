;(function(context) {

		var getMagnificItemForEl = function(el) {
			var 
				magnificItem = {},
				type = el.data('type');
				magnificItem.src = el[0].href || el.data('src');
				magnificItem.title = el.data('title');
				
				//a little help
				if(magnificItem.src.match(/vimeo|youtube|maps/i)) {
					type = 'iframe';
				} else if(magnificItem.src.match(/(jpg|gif|png|jpeg)(\?.*)?$/i)) {
					type = 'image';
				}				
				
			switch(type) {
				case 'image':
					magnificItem.type = 'image';
					break;
				case 'inline':
					magnificItem.type = 'inline';
					magnificItem.src = $(magnificItem.src).html();
					break;
				case 'ajax':
					magnificItem.type = 'ajax';
					break;
				case 'iframe':
				case 'video':
				case 'map':
					magnificItem.type = 'iframe';
					break;
			}
			
			return magnificItem;
		};

		//magnific popup
		$(document)
			.on('click','.mpopup',function(e) {
				e.preventDefault();
				var 
					magnificItems,
					items,
					isPartOfGallery,
					thisIndex = 0,
					el = $(this),
					galleryName = el.data('gallery');
					
					isPartOfGallery = !!galleryName;
				
				if(isPartOfGallery) {
					magnificItems = [];
					items = $('.mpopup').filter(function(i) { return $(this).data('gallery') === galleryName; });
					
					items.each(function(i) { 
						if(this === el[0]) { 
							thisIndex = i;  
						} 
						magnificItems.push(getMagnificItemForEl($(this)));
						return true; 
					});
				} else {
					magnificItems = getMagnificItemForEl(el);
				}
				
				$.magnificPopup.open({
					items:magnificItems,
					disableOn: 700,
					mainClass: 'mfp-zoom-in',
					removalDelay: 160,
					preloader: true,
					fixedContentPos: true,
					gallery:{enabled:isPartOfGallery}
				},thisIndex);
			
			});

}(typeof ns !== 'undefined' ? window[ns] : undefined));