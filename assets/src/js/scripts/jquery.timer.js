;(function(context) {

		$.fn.countdown = function( action, options) {
			
			return this.each( function ( i, element ) {
				var 
					el = $(element),
					timer = el.data('timer');
					
				if ( action === undefined || action == 'init' ) {
					
					timer = new Timer(el);
					timer.start();
					el.data('timer',timer);
					
				} else if(typeof timer[ action ] === 'function') {
					timer[ action ]( options )
				}
			});
			
		};
		
		var
			ms = 1,
			s = ms*1000,
			m = s*60,
			h = m*60,
			d = h*24,
			pad = function(num,len,padWith) {
			
				var strNum = num+'';
				while(strNum.length < len) {
					strNum = (padWith+'')+strNum;
				}
			
				return strNum;
			};
		
		var Timer = function(el) {
			this.interval = null;
			this.target = new Date(el.data('target').valueOf());
			this.el = el;
		};
		
		Timer.prototype.start = function() {
			var self = this;
			
			//start immediately
			self.display();
			
			this.interval = setInterval(function() {
				self.display();
			},1000);
		
		};
		
		Timer.prototype.display = function() {
		
			var 
				days,
				hours,
				minutes,
				seconds,
				milliseconds,
				now = new Date().valueOf(),
				duration = this.target-now;
				
				days = ( Math.floor( duration/d ) ).toFixed(0);
					duration -= days*d
				hours = ( Math.floor( duration/h ) ).toFixed(0);
					duration -= hours*h;
				minutes = ( Math.floor( duration/m ) ).toFixed(0);
					duration -= minutes*m;
				seconds = ( Math.floor( duration/s ) ).toFixed(0);
					duration -= seconds*s;
				milliseconds = duration;
				
				this
					.el
					.find('.days .val')
					.html(pad(days,3,"0"))
					.end()
					.find('.hours .val')
					.html(pad(hours,2,"0"))
					.end()
					.find('.minutes .val')
					.html(pad(minutes,2,"0"))
					.end()
					.find('.seconds .val')
					.html(pad(seconds,2,"0"));
		
		};
		
		Timer.prototype.stop = function() {
			clearInterval(this.interval);
		};

}(typeof ns !== 'undefined' ? window[ns] : undefined));