;(function(context) {

	
	var gmap;
	var customInfoWindow;
	var tim;
	
	if(context) {
		customInfoWindow = context.CustomInfoWindow;
		tim = context.tim;
		gmap = context.gmp;
	} else {
		customInfoWindow = require('./map/custom.infowindow.js');
		tim = require('./tim.microtemplate.js');
		gmap = require('./gmap.js');
	}

	var 
		$document = $(document),
		$el = $('.location-section'),
		template = '\
			<span class="t-fa-abs fa-close">Close</span>\
			<div class="if-wrap">\
				<div class="contact">\
					<address>{{address}}</address>\
					{{phoneFormat}}\
					{{faxFormat}}\
				</div>\
				<div class="sv-image" style="background-image: url(//maps.googleapis.com/maps/api/streetview?location={{lat}},{{lng}}&size=270x134);">\
				</div>\
			</div>\
			<a href="{{link}}" rel="external" class="button block secondary">View More Information</a>\
		',
		locationMapMethods = {
			
			init: function(map) {
				this.map = map;
				this.customInfoWindow = customInfoWindow({},map.map);
			},
			
			getMarkerFromElement: function($markerEl) {
				var mapMarker = null;
				$.each(this.map.markers,function(i,marker) {
					mapMarker = marker.getMarkerDiv() === $markerEl[0] ? marker : false;
					return !mapMarker;
				});
				return mapMarker;
			},
			
			selectMarkerByIndex: function(i) {
				if(typeof i !== 'undefined' && this.map.markers[i]) {
					this.map.map.setCenter(this.map.markers[i].getPosition());
					this.selectMarker($(this.map.markers[i].getMarkerDiv()));
				} else {
					this.closeInfoWindow();
					this.fitToBounds();
				}
			},
			
			fitToBounds: function() {
				var bounds = new google.maps.LatLngBounds();
				$.each(this.map.markers,function(i,marker) { bounds.extend(marker.getPosition()); });
				this.map.map.fitBounds(bounds);
			},
			
			selectMarker: function($markerEl) {
				var mapMarker,data;
				$.each(this.map.markers,function(i,marker) {
					marker.setSelected(marker.getMarkerDiv() === $markerEl[0]);
					mapMarker = marker.isSelected() ? marker : mapMarker;
				});
				
				data = mapMarker.getData();
				data.lat = mapMarker.getPosition().lat();
				data.lng = mapMarker.getPosition().lng();
				data.phoneFormat = '<span class="block">p: '+data.phone+'</span>';
				data.faxFormat = '<span class="block">f: '+data.phone+'</span>';
				
				
				this.customInfoWindow.setContent(tim(template,data));
				this.customInfoWindow.open(mapMarker.getPosition());
				
			},
			
			closeInfoWindow: function() {
				this.customInfoWindow.hide();
				$.each(this.map.markers,function(i,marker) {
					marker.setSelected(false);
				});
			}
		};
		
	$el
		.on('mapLoad',function(e,map) {
			locationMapMethods.init(map);
		})
		.on('click','.crosbie-map-marker',function() {
			locationMapMethods.selectMarker($(this));
		})
		.on('change','.location-selector',function() {
			locationMapMethods.selectMarkerByIndex(this.selectedIndex-1);
		});
		
	$document
		.on('click','.custom-info-window .fa-close',function() {
			console.log('here');
			locationMapMethods.closeInfoWindow();
		});

}(typeof ns !== 'undefined' ? window[ns] : undefined));