<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-quote.php'); ?>
	
	<?php include('inc/i-fast-fact.php'); ?>

	<div class="ov-grid">
		<a class="ov-item bounce" href="#">
			<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-1.jpg"></div>
			<span class="ov-title fa-car">Auto <br /> Insurance</span>
		</a><!-- .ov-item -->
		<a class="ov-item bounce" href="#">
			<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-2.jpg"></div>
			<span class="ov-title fa-home">Home <br />Insurance</span>
		</a><!-- .ov-item -->
		<a class="ov-item bounce" href="#">
			<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-3.jpg"></div>
			<span class="ov-title fa-building-o">Commercial <br />Insurance</span>
		</a><!-- .ov-item -->
		<a class="ov-item bounce" href="#">
			<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-4.jpg"></div>
			<span class="ov-title fa-ship">Marine <br />Insurance</span>
		</a><!-- .ov-item -->
	</div><!-- .ov-grid -->

	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>