<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-3.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Contact</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. 
								Suspendisse fermentum arcu lacus, vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. 
								Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl interdum id.
							</p>

							<p>
								Donec pulvinar quam risus, at laoreet neque tempor eget. Nunc rhoncus eu lacus eget vulputate. Suspendisse lobortis ultrices nunc, eu accumsan ex lacinia sed. 
								Quisque odio ante, tincidunt nec faucibus vitae, cursus a orci. Maecenas aliquam ultricies tristique. Fusce dictum lectus diam, mattis malesuada nunc pulvinar in.
							</p>
							
							<div class="acc with-indicators separated allow-multiple">
							
								<div class="acc-item">
									<div class="acc-item-handle">
										Praesent consectetur augue leo, quis ultricies orci porta ut?
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">
										Praesent consectetur augue leo, quis ultricies orci porta ut?
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">
										Praesent consectetur augue leo, quis ultricies orci porta ut?
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">
										Praesent consectetur augue leo, quis ultricies orci porta ut?
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">
										Praesent consectetur augue leo, quis ultricies orci porta ut?
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								
							</div><!-- .acc -->
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						
						<?php include('inc/i-claim-mod.php'); ?>
						
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>