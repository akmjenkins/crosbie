<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-3.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Executive</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. Suspendisse fermentum arcu lacus, 
								vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl 
								interdum id.
							</p>

							<p>
								Donec pulvinar quam risus, at laoreet neque tempor eget. Nunc rhoncus eu lacus eget vulputate. Suspendisse lobortis ultrices nunc, eu accumsan ex lacinia sed. Quisque odio 
								ante, tincidunt nec faucibus vitae, cursus a orci. Maecenas aliquam ultricies tristique. Fusce dictum lectus diam, mattis malesuada nunc pulvinar in.
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						<?php include('inc/i-claim-mod.php'); ?>
					</aside>
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="ov-section grey-bg">
	
		<div class="hgroup centered">
			<h2 class="hgroup-title">Executive</h2>
		</div><!-- .hgroup -->
	
		<div class="ov-grid circle-grid">
			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-1.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-2.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-3.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-4.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-5.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->
			
			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-6.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			
		</div><!-- .ov-grid -->
	</section>

	<section class="ov-section dark-bg">
	
		<div class="hgroup centered">
			<h2 class="hgroup-title">Executive</h2>
		</div><!-- .hgroup -->
	
		<div class="ov-grid circle-grid">
			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-1.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-2.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-3.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-4.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-5.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->
			
			<div class="ov-circle-item">
				<div class="img-wrap">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/tm-6.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Executive Member</span>
					<small class="block">Job Title</small>
					<a href="#" class="button secondary">Read More</a>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->

			
		</div><!-- .ov-grid -->
	</section>
	
	<?php include('inc/i-location-section.php'); ?>
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>