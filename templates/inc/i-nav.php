<div class="nav">

	<div class="nav-toggle-wrap">
		<button class="toggle-nav">
			<span>&nbsp;</span> Menu
		</button>
	</div><!-- .nav-toggle-wrap -->
	
	<div class="nav-off-canvas-wrap">
		<div class="nav-off-canvas dark-bg">
			
			<nav>
				<ul>
					<li><a href="#">Auto</a></li>
					<li><a href="#">Home</a></li>
					<li><a href="#">Commercial</a></li>
					<li><a href="#">Marine</a></li>
					<li><a href="#">Other</a></li>
					<li><a href="#">Locations</a></li>
					<li><a href="#">Our History</a></li>
					<li><a href="#">Our Executive</a></li>
					<li class="drop">
						<a href="#">Claims</a>
						<ul>
							<li><a href="#">Auto Claim</a></li>
							<li><a href="#">Home Claim</a></li>
							<li><a href="#">Commerical Claim</a></li>
							<li><a href="#">Marine Claim</a></li>
						</ul>
					</li><!-- .drop -->
				</ul>
			</nav>
			
			<a href="#" class="button block secondary">Get A Quote</a>
		
		</div><!-- .off-canvas -->
	</div><!-- .nav-off-canvas-wrap -->
	
	<div class="nav-top">
		<a href="#">About</a>
		<a href="#">FAQ</a>
		<a href="#">Contact</a>
		<?php include('i-social.php'); ?>
		
		<div class="t-fa fa-info-circle contact-item">
			<div class="contact-card">
				<div>
					<span class="title">Contact Us</span>
					
					<address>
						1 Crosbie Place <br />
						St. John's, NL A1B 3Y8
					</address>
					
					<span class="phone">
						<span>Ph</span>(709) 123-4567
					</span><!-- .phone -->
					
					<span class="phone">
						<span>TF</span>(800) 123-4567
					</span><!-- .phone -->
				</div>
			</div><!-- .contact-card -->
		</div><!-- .t-fa -->
		
		<form class="single-form global-search-form" action="/">
			<div class="fieldset">
				<input type="text" name="s" placeholder="Search">
				<button class="t-fa fa-search">&nbsp;</button>		
				<button type="button" class="t-fa fa-close toggle-search search-close">&nbsp;</button>
			</div><!-- .fieldset -->
		</form><!-- .single-form -->
		
	</div><!-- .nav-top -->
	
</div><!-- .nav -->