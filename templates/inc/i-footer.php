			<footer class="dark-bg">
				
				<div class="footer-blocks">
				
					<div class="footer-block footer-nav">
					
						<ul>
							<li><a href="#">Auto</a></li>
							<li><a href="#">Home</a></li>
							<li><a href="#">Commercial</a></li>
							<li><a href="#">Marine</a></li>
							<li><a href="#">Other</a></li>
							<li><a href="#">Locations</a></li>
						</ul>
						
						<?php include('i-social.php'); ?>
					
					</div><!-- .footer-block -->
					
					<div class="footer-block footer-claim-wrap">
					
						<div class="footer-make-claim">
							<div class="lazybg" data-src="../assets/dist/images/temp/footer-1.jpg">&nbsp;</div>
							<div>
								<span class="text">In an accident? Find out how you can make a claim today.</span>
								
								<a href="#" class="button secondary">Make a Claim</a>
							</div>
						</div><!-- .footer-make-claim -->
					
					</div><!-- .footer-block -->
					
					<div class="footer-block footer-contact">
							
						<div>
						
							<div class="address-phone">
							
								<address>
									123 This Street <br />
									St. John's, NL <br />
									A1B 2C3
								</address>
								
								<div class="phone">
									<span class="block">709 123 4567</span>
									<span class="block">709 123 8910</span>
								</div><!-- .phone -->
								
							</div><!-- .address-phone -->
						
							<a href="#" class="button secondary">Contact Us</a>
						</div>
						
						<a href="#" rel="external" class="logo">
							<img src="../assets/dist/images/bip-logo.svg">
						</a>
					
					</div><!-- .footer-block -->
				
				</div><!-- .footer-blocks -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Crosbie Job Insurance</a> All Rights Reserved.</li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/crosbie',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>