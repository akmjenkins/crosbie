<div class="latest-updates">

	<h3>Latest Updates</h3>
	
	<div class="swiper-wrapper">
		<div class="swiper">
			<div class="swipe-item">
				<p>
					Lorem <a href="#link" class="inline visited">@ipsum</a> dolor sit amet, consectetur adipiscing elit. Fusce faucibus 
					diam id sapien dapibus molestie. Nulla <a href="#link" class="inline">#luctus</a> <a href="#link" class="inline">#arcu</a>
				</p>
			</div><!-- .swipe-item -->
			<div class="swipe-item">
				<p>
					Lorem <a href="#link" class="inline">@ipsum</a> dolor sit amet, consectetur adipiscing elit. Fusce faucibus 
					diam id sapien dapibus molestie. Nulla <a href="#link" class="inline">#luctus</a> <a href="#link" class="inline">#arcu</a>
				</p>
			</div><!-- .swipe-item -->
		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .latest-updates -->