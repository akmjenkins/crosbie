<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Crosbie Job Insurance</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../assets/dist/lib/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- pt sans -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet'>
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/dist/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/dist/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/dist/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/dist/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/dist/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/dist/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/dist/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/dist/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/dist/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/dist/images/favicons/favicon-144.png">

		<!-- slickslider -->
		<link rel="stylesheet" href="../assets/dist/lib/slick.js/slick/slick.css">
		<script src="../assets/dist/lib/slick.js/slick/slick.min.js"></script>
		
		<!-- magnific popup -->
		<link rel="stylesheet" href="../assets/dist/lib/magnific-popup/dist/magnific-popup.css">
		<script src="../assets/dist/lib/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		
		<!-- datetimepicker -->
		<link rel="stylesheet" href="../assets/dist/lib/datetimepicker/jquery.datetimepicker.css">
		<script src="../assets/dist/lib/datetimepicker/jquery.datetimepicker.js"></script>
		
		<link rel="stylesheet" href="../assets/dist/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
		
		<header>
			<div class="sw">
				<a href="#" class="header-logo lazybg with-img">
					<img src="../assets/dist/images/crosbie-job-insurance.png" alt="Crosbie Job Insurance Logo">
				</a>
				
				<span class="phone">
					Call Us 1 (709)  726-5414
				</span><!-- .phone -->
				
			</div><!-- .sw -->
		</header>
	
		<div class="page-wrapper">	