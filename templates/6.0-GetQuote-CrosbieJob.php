<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-3.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Our History</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. 
								Suspendisse fermentum arcu lacus, vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. 
								Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl interdum id.
							</p>

							<p>
								Donec pulvinar quam risus, at laoreet neque tempor eget. Nunc rhoncus eu lacus eget vulputate. Suspendisse lobortis ultrices nunc, eu accumsan ex lacinia sed. 
								Quisque odio ante, tincidunt nec faucibus vitae, cursus a orci. Maecenas aliquam ultricies tristique. Fusce dictum lectus diam, mattis malesuada nunc pulvinar in.
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						
						<?php include('inc/i-claim-mod.php'); ?>
						
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg quote-form">
		<div class="sw">
		
			<div class="hgroup">
				<h2 class="hgroup-title">Name &amp; Address</h2>
				<span>* All fields are required</span>
			</div><!-- .hgroup -->
				
			<form action="/" class="body-form full">
				<div class="grid collapse-900">
					<div class="col col-2">
						<div class="item">
							<h5 class="fieldset-title">Name</h5>
							<div class="fieldset">
								<div class="grid pad10 collapse-650">
									<div class="col col-2">
										<div class="item">
											<input type="text" name="fname" placeholder="First Name">		
										</div>
									</div><!-- .col -->
									<div class="col col-2">
										<div class="item">
											<input type="text" name="lname" placeholder="Last Name">		
										</div>
									</div><!-- .col -->
									<div class="col col-2">
										<div class="item">
											<input type="text" name="mi" placeholder="Middle Initial">		
										</div>
									</div><!-- .col -->
									<div class="col col-2">
										<div class="item">
											<input type="text" name="suffix" placeholder="Suffix">		
										</div>
									</div><!-- .col -->
									<div class="col col-1">
										<div class="item">
											<label class="field-wrap">
												<span class="l">Date of Birth</span>
												<input type="text" class="date-input" 
													data-max-date="<?php
														//e.g. you must be at least 18 years of age to apply for an insurance policy...I made this up just to demonstrate how the datepicker options work
														$now = new DateTime();
														$interval = new DateInterval('P18Y');
														$then = $now->sub($interval);
														echo $then->format('Y/m/d');
													?>" 
													max="<?php echo $then->format('Y-m-d'); ?>"
													data-default-date="<?php echo $then->format('Y/m/d'); ?>"
													data-timepicker="false" 
													data-format="Y-m-d" 
													name="dob">
											</label>
										</div><!-- .item -->
									</div><!-- .col -->
								</div><!-- .grid.pad10 -->
							</div><!-- .fieldset -->
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2">
						<div class="item">
							<h5 class="fieldset-title">Address</h5>
							<div class="fieldset">
								<div class="grid eqh pad10 collapse-650">
									<div class="col col-1">
										<div class="item">
											<input type="email" name="email" placeholder="Email Address">			
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-2">
										<div class="item">
											<input type="tel" name="home_phone" placeholder="Home Phone">			
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-2">
										<div class="item">
											<input type="tel" name="alt_phone" placeholder="Alternate Phone">
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-2-3">
										<div class="item">
											<input type="text" name="mailing_address" placeholder="Mailing Address">			
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="apt" placeholder="Apartment/Unit #">
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="city" placeholder="City">
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-2-3">
										<div class="item">
											<div class="selector with-arrow">
												<select name="province">
													<option value="AB">Alberta</option>
													<option value="BC">British Columbia</option>
													<option value="MB">Manitoba</option>
													<option value="NB">New Brunswick</option>
													<option value="NL">Newfoundland and Labrador</option>
													<option value="NS">Nova Scotia</option>
													<option value="ON">Ontario</option>
													<option value="PE">Prince Edward Island</option>
													<option value="QC">Quebec</option>
													<option value="SK">Saskatchewan</option>
													<option value="NT">Northwest Territories</option>
													<option value="NU">Nunavut</option>
													<option value="YT">Yukon</option>
												</select>
												<span class="value">&nbsp;</span>
											</div><!-- .selector -->
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="postal" placeholder="Postal Code">
										</div><!-- .item -->
									</div><!-- .col -->

									
								</div><!-- .grid -->
							
							</div><!-- .fieldset -->
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
				
				
			</form><!-- .body-form -->

			<h3>Information Disclosure</h3>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor 
				sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus 
				et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra.
			</p>
			
			<div class="quote-tabs tab-wrapper">
				<div class="tab-controls">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option value="Auto">Auto</option>
							<option value="Home">Home</option>
							<option value="Commercial">Commercial</option>
							<option value="Marine">Marine</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				
					<button class="tab-control selected">Auto</button>
					<button class="tab-control">Home</button>
					<button class="tab-control">Commercial</button>
					<button class="tab-control">Marine</button>
				
				</div><!-- .tab-controls -->
				<div class="tab-holder">
					
					<div class="tab selected">
					
						<form action="/" class="body-form full">
						
							<div class="hgroup">
								<h3 class="hgroup-title">Licence &amp; Registration</h3>
								<p>* All fields are required.</p>
							</div><!-- ..hgroup -->
								
							<div class="fieldset">
								<div class="grid pad10 collapse-650">
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="licence_no" placeholder="Drivers Licence Number">		
										</div>
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<label class="field-wrap">
												<span class="l">Expiry Date</span>
												<input type="text" class="date-input" 
													data-timepicker="false" 
													data-format="Y-m-d" 
													data-min-date="<?php
														$now = new DateTime();
														echo $now->format('Y/m/d');
													?>"
													min="<?php echo $now->format('Y-m-d'); ?>"
													name="licence_expiry">
											</label>
										</div><!-- .item -->
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="plate_no" placeholder="Plate Number">		
										</div><!-- .item -->
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<div class="selector with-arrow">
												<select name="model">
													<option value="">Model</option>
													<option value="AB">Model 1</option>
													<option value="BC">Model 2</option>
												</select>
												<span class="value">&nbsp;</span>
											</div><!-- .selector -->											
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-1-3">
										<div class="item">
											<div class="selector with-arrow">
												<select name="manufacturer">
													<option value="">Manufacturer</option>
													<option value="AB">Model 1</option>
													<option value="BC">Model 2</option>
												</select>
												<span class="value">&nbsp;</span>
											</div><!-- .selector -->											
										</div><!-- .item -->
									</div><!-- .col -->

									<div class="col col-1-3">
										<div class="item">
											<input type="number" placeholder="Year">		
										</div><!-- .item -->
									</div><!-- .col -->
									
									<div class="col col-1-3">
										<div class="item">
											<div class="selector with-arrow">
												<select name="manufacturer">
													<option value="">Colour</option>
													<option value="AB">Model 1</option>
													<option value="BC">Model 2</option>
												</select>
												<span class="value">&nbsp;</span>
											</div><!-- .selector -->											
										</div><!-- .item -->
									</div><!-- .col -->


								</div><!-- .grid.pad10 -->
							</div><!-- .fieldset -->

							<hr />
							
							<div class="hgroup">
								<h3 class="hgroup-title">Additional Drivers</h3>
								<p>* All fields are required.</p>
							</div><!-- ..hgroup -->
							
							<div class="fieldset">
								<div class="grid pad10 collapse-650">
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="fname" placeholder="First Name">		
										</div>
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="lname" placeholder="Last Name">		
										</div>
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<div class="grid pad10">
												<div class="col col-2">
													<div class="item">
														<input type="text" name="mi" placeholder="Middle Initial">		
													</div>
												</div><!-- .col -->
												<div class="col col-2">
													<div class="item">
														<input type="text" name="suffix" placeholder="Suffix">		
													</div>
												</div><!-- .col -->
											</div><!-- .grid -->
										</div><!-- .item -->
									</div>
									<div class="col col-1-3">
										<div class="item">
											<label class="field-wrap">
												<span class="l">Date of Birth</span>
												<input type="text" class="date-input" 
													data-max-date="<?php
														//e.g. in NL, you have to be at least 16 years, 8 months of age to be eligible for a drivers license
														$now = new DateTime();
														$interval = new DateInterval('P16Y8M');
														$then = $now->sub($interval);
														echo $then->format('Y/m/d');
													?>" 
													max="<?php echo $then->format('Y-m-d'); ?>"
													data-default-date="<?php echo $then->format('Y/m/d'); ?>"
													data-timepicker="false" 
													data-format="Y-m-d" 
													name="dob">
											</label>
										</div><!-- .item -->
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<input type="text" name="licence_no" placeholder="Drivers Licence Number">		
										</div>
									</div><!-- .col -->
									<div class="col col-1-3">
										<div class="item">
											<label class="field-wrap">
												<span class="l">Expiry Date</span>
												<input type="text" class="date-input" 
													data-min-date="<?php
														$now = new DateTime();
														echo $now->format('Y/m/d');
													?>" 
													min="<?php echo $then->format('Y-m-d'); ?>"
													data-default-date="<?php echo $now->format('Y/m/d'); ?>"
													data-timepicker="false" 
													data-format="Y-m-d" 
													name="licence_expiry">
											</label>
										</div><!-- .item -->
									</div><!-- .col -->
								</div><!-- .grid.pad10 -->
							</div><!-- .fieldset -->
							
							<div class="cf">
								<button class="f-right button with-ico t-fa fa-plus-circle secondary">Add Another Driver</button>	
							</div><!-- .add-driver -->
							
							
							<button class="button secondary" type="submit">Submit</button>
							
						</form><!-- .body-form -->
						
					
					</div><!-- .tab -->
					
					<div class="tab">
						<div class="hgroup">
							<h3 class="hgroup-title">Home</h3>
							<p>* All fields are required.</p>
						</div><!-- ..hgroup -->
					</div><!-- .tab -->
					
					<div class="tab">
						<div class="hgroup">
							<h3 class="hgroup-title">Commercial</h3>
							<p>* All fields are required.</p>
						</div><!-- ..hgroup -->
					</div><!-- .tab -->
					
					<div class="tab">
						<div class="hgroup">
							<h3 class="hgroup-title">Marine</h3>
							<p>* All fields are required.</p>
						</div><!-- ..hgroup -->
					</div><!-- .tab -->
					
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
		
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
	<section class="d-bg primary-bg">
		<div class="sw full">
			<?php include('inc/i-latest-updates.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>