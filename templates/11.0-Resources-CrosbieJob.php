<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Resources</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. Suspendisse fermentum arcu lacus, 
								vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl 
								interdum id.
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						<?php include('inc/i-claim-mod.php'); ?>
					</aside>
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<div class="hgroup centered">
				<h2 class="hgroup-title">Documents &amp; Policies</h2>
			</div><!-- .hgroup-centered -->
			
			<div class="filter-section">
				
				<div class="filter-bar">
					<div class="filter-bar-content">				
						<div class="filter-bar-left">
							<div class="count">
								<span class="num">6</span> Documents Found
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->
						
						<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					</div><!-- .filter-bar-content -->
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					
					<div class="grid eqh filter-items">
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item">								
								<h4 class="item-title">Document Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies sit amet ex quis dapibus. In vehicula vitae elit ac porta. Ut tempus facilisis ultricies.</p>
								
								<span class="button secondary block">Download</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item">								
								<h4 class="item-title">A Much Longer Document Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies</p>
								
								<span class="button secondary block">Download</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item">								
								<h4 class="item-title">Document Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean </p>
								
								<span class="button secondary block">Download</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item">								
								<h4 class="item-title">A Much Longer Document Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies sit amet ex quis dapibus. In vehicula vitae elit ac porta. Ut tempus facilisis ultricies.</p>
								
								<span class="button secondary block">Download</span>
							</a><!-- .item -->
						</div><!-- .col -->


					</div><!-- .grid -->
					
				</div><!-- .filter-content -->
				
			</div><!-- .filter-section -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg">
		<div class="sw">
		
			<div class="hgroup centered">
				<h2 class="hgroup-title">Helpful Links</h2>
			</div><!-- .hgroup-centered -->
			
			<div class="filter-section">
				
				<div class="filter-bar">
					<div class="filter-bar-content">				
						<div class="filter-bar-left">
							<div class="count">
								<span class="num">10</span> Links Found
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->
						
						<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					</div><!-- .filter-bar-content -->
				</div><!-- .filter-bar -->
				
				<div class="filter-content">
					
					<div class="grid eqh filter-items">
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item" rel="external">	 							
								<h4 class="item-title">Link Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies sit amet ex quis dapibus. In vehicula vitae elit ac porta. Ut tempus facilisis ultricies.</p>
								
								<span class="button secondary block">Visit</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item" rel="external">	 															
								<h4 class="item-title">A Much Longer Link Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies</p>
								
								<span class="button secondary block">Visit</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item" rel="external">	 													
								<h4 class="item-title">Link Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean </p>
								
								<span class="button secondary block">Visit</span>
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item" rel="external">	 													
								<h4 class="item-title">A Much Longer Link Title</h4>
							
								<p>In eu laoreet libero, nec fermentum eros. Aenean ultricies sit amet ex quis dapibus. In vehicula vitae elit ac porta. Ut tempus facilisis ultricies.</p>
								
								<span class="button secondary block">Visit</span>
							</a><!-- .item -->
						</div><!-- .col -->


					</div><!-- .grid -->
					
				</div><!-- .filter-content -->
				
			</div><!-- .filter-section -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>