<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-3.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Single Location Title</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. 
								Suspendisse fermentum arcu lacus, vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. 
								Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl interdum id. 
							</p>

							<p>
								Morbi viverra congue magna, a viverra lectus venenatis ac. Fusce neque tortor, malesuada in pretium a, suscipit id quam. Etiam dictum ante elit, 
								at volutpat dui tempus ut. Integer interdum, odio non congue laoreet, dolor diam vehicula lectus, vel interdum nunc nunc at massa. Quisque porttitor 
								convallis laoreet. Aenean auctor consequat mauris, et congue nunc. Vivamus consequat pharetra turpis, at ullamcorper justo fermentum eu. Aenean 
								consectetur, sapien sit amet hendrerit tempor, ante dui venenatis felis, vitae vulputate libero quam in sapien.
							</p>

							<p>
								Donec viverra quam vitae tortor ultrices egestas. Mauris fermentum pretium lobortis. Nam vel erat ipsum. Integer congue bibendum metus non interdum. 
								Vestibulum ac pellentesque sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi commodo diam 
								id tellus dapibus, in laoreet diam pretium. Praesent maximus sollicitudin vestibulum. Sed eget elit sed purus accumsan vestibulum quis et nunc. Morbi 
								sed luctus sem. Nulla fermentum odio at felis vulputate laoreet 
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						
						<div class="mod acc-mod">
						
							<div class="acc with-indicators">
								<div class="acc-item">
									<div class="acc-item-handle">Select Location</div>
									<div class="acc-item-content">
									
										<ul>
											<li><a href="#">Location One</a></li>
											<li><a href="#">Location Two</a></li>
											<li><a href="#">Location Three</a></li>
										</ul>
									
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->
							</div><!-- .acc -->
							
							<a href="#" class="button block">Back to Locations</a>
							
						</div><!-- .mod -->
						
						<div class="mod contact-mod">
						
							<strong class="uc block">Contact Person</strong>
							
							<br />
							
							<strong>Address</strong>
							<address>
								123 This Street <br />
								This City, NL A1B 2C3
							</address>
							
							<br />
							
							<span class="block t-fa fa-phone">709 123 4567</span>
							<a href="#" class="block t-fa fa-envelope">person@crosbiejobs.com</a>
						
						</div><!-- .mod -->
						
						<?php include('inc/i-claim-mod.php'); ?>
						
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="ov-section">
	
		<div class="ov-grid">
			<a class="ov-item bounce" href="#">
				<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-1.jpg"></div>
				<span class="ov-title fa-car">Auto <br /> Insurance</span>
			</a><!-- .ov-item -->
			<a class="ov-item bounce" href="#">
				<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-2.jpg"></div>
				<span class="ov-title fa-home">Home <br />Insurance</span>
			</a><!-- .ov-item -->
			<a class="ov-item bounce" href="#">
				<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-3.jpg"></div>
				<span class="ov-title fa-building-o">Commercial <br />Insurance</span>
			</a><!-- .ov-item -->
			<a class="ov-item bounce" href="#">
				<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-4.jpg"></div>
				<span class="ov-title fa-ship">Marine <br />Insurance</span>
			</a><!-- .ov-item -->
		</div><!-- .ov-grid -->

	</section>
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>