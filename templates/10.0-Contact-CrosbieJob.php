<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-3.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Contact</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Ut imperdiet euismod enim, ac maximus neque pharetra nec. Quisque faucibus scelerisque facilisis. Curabitur viverra eget arcu vel sagittis. 
								Suspendisse fermentum arcu lacus, vitae pulvinar nulla pellentesque eu. Nunc et scelerisque turpis, id blandit nunc. Vestibulum quis mattis libero. 
								Nunc non elementum mi. Mauris feugiat ipsum felis, non porta nisl interdum id.
							</p>

							<p>
								Donec pulvinar quam risus, at laoreet neque tempor eget. Nunc rhoncus eu lacus eget vulputate. Suspendisse lobortis ultrices nunc, eu accumsan ex lacinia sed. 
								Quisque odio ante, tincidunt nec faucibus vitae, cursus a orci. Maecenas aliquam ultricies tristique. Fusce dictum lectus diam, mattis malesuada nunc pulvinar in.
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
					
					<aside class="sidebar">
						
						<?php include('inc/i-claim-mod.php'); ?>
						
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<div class="contact-grid">
			
				<div class="contact-grid-address">
					<h4>Address</h4>
					
					<address>
						1 Crosbie Pl <br />
						St. John's, NL A1B 3Y8
					</address>
					
					<br />
					
					<span class="block">p. (709) 123-4567</span>
					<span class="block">f. (709) 123-5678</span>
				</div><!-- .contact-grid-address -->
				
				<div class="contact-grid-form">
					
					<form action="/" class="body-form">
						<div class="fieldset">
						
							<input type="text" name="name" placeholder="Name">
							<input type="tel" name="phone" placeholder="Phone">
							<input type="email" name="email" placeholder="Email">
							<textarea name="message" placeholder="Message"></textarea>
							
							<button class="button secondary">Submit</button>
						
						</div><!-- .fieldset -->
					</form>
					
				</div><!-- .contact-grid-form -->
			
			</div><!-- .contact-grid -->
		
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>