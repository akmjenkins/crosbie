<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
		
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg, http://dummyimage.com/1200x500/000/fff 1200w, http://dummyimage.com/600x500/000/fff 600w"></div>
			
		</div><!-- .fader-item -->
	</div><!-- .fader -->
		
</div><!-- .hero -->

<div class="body">

	<?php include('inc/i-quote.php'); ?>

	<section>
		<div class="sw">
		
			<article>
				<div class="main-body">				
					<div class="content">
						
						<div class="content-header hgroup">
							<h1 class="hgroup-title">Auto Insurance</h1>
							<span class="hgroup-subtitle">Donec pulvinar quam risus, at laoreet neque</span>
						</div><!-- .content-header -->
						
						<div class="article-body">
						
							<p>
								Vivamus aliquet ex eu interdum vehicula. Nam ut ullamcorper ante. Ut bibendum scelerisque est non pellentesque. 
								Fusce fringilla efficitur arcu, nec venenatis ante egestas et. Donec a finibus ligula. Donec non arcu molestie, 
							</p>
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
				</div><!-- .main-body -->
			</article>

		
		</div><!-- .sw -->
	</section>

	<?php include('inc/i-fast-fact.php'); ?>
	
	<section class="ov-section">
	
		<div class="hgroup centered">
			<h2 class="hgroup-title">Vestibulum dui mi euismod sit amet</h2>
			<p>
				Nullam ornare enim eu tortor porta efficitur. Vivamus vehicula, orci id molestie porta, nunc lectus placerat libero, eget scelerisque metus velit.
			</p>
		</div><!-- .hgroup -->
	
		<div class="ov-grid circle-grid">
			<div class="ov-circle-item">
				<div class="img-wrap no-500">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-1.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Feature One</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
				</div><!-- .ov-circle-content -->
				
			</div><!-- .ov-circle-item -->
			
			<div class="ov-circle-item">
				<div class="img-wrap no-500">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-5.jpg"></div>	
				</div><!-- .img-wrap -->
				<div class="ov-circle-content">
					<span class="ov-circle-title">Feature Two</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
				</div><!-- .ov-circle-content -->
			</div><!-- .ov-circle-item -->
			
			<div class="ov-circle-item">
				<div class="img-wrap no-500">
					<div class="lazybg img"  data-src="../assets/dist/images/temp/ov-6.jpg"></div>	
				</div><!-- .img-wrap -->
				
				<div class="ov-circle-content">
					<span class="ov-circle-title">Feature Three</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
				</div><!-- .ov-circle-content -->
			</div><!-- .ov-circle-item -->
			
		</div><!-- .ov-grid -->
	</section>
	
	<section class="d-bg primary-bg">
		<div class="sw full">
		
			<?php include('inc/i-latest-updates.php'); ?>
		
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>